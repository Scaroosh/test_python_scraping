import requests
import json

##### EXERCICE 1 #####

def http_request():
    url = 'https://httpbin.org/anything'
    body = {'isadmin': 1, 'User-Agent': ''} # Body

    req = requests.post(url, data = body) # POST Request
    return (req) # Returns not the body but the req, because it is easier to unit test, else it would be req.content

##### EXERCICE 2 #####

class Store:
    def __init__(self): # Initialize, loads data.json into self.data, or raise an Exception if something wrong occurs
        try:
            with open("./third_part/data/data.json", "r") as read_file:
                self.data = json.load(read_file)
        except:
            raise Exception("File not found or .gz not extracted.")

    def truncate_and_clean(self, name): # Cleaning the name so it looks like the required format
        bannedList = ['Months', 'months', 'kg']
        name_l = name.split(' ')
        for i in range(0, len(name_l)):
            if name_l[i].isdigit() or any (x in name_l[i] for x in bannedList):
                name = ' '.join(name_l[0:i])
        name = ' '.join(name_l)
        if len(name) > 30:
            name = name[0:30]
        return (name)

    def write_in_csv(self, string):
        file = open("Store.csv", "a")
        file.write(string)
        file.close()

    def print_products(self):
        amount = 0 # To show how many articles is available
        # Loop every articles in Bundles
        for i in range (0, len(self.data['Bundles'])):
            # Check if the second item should be 'Product' or 'Products'
            ProductTag = 'None'
            if 'Product' in self.data['Bundles'][i]:
                ProductTag = 'Product'
            elif 'Products' in self.data['Bundles'][i]:
                ProductTag = 'Products'
            # If it didn't find Product or Products, it means that the product is not available and doesn't contain a reason nor stockcode, so we just print it with an error
            if ProductTag == 'None':
                if 'Name' in self.data['Bundles'][i]:
                    self.write_in_csv(self.truncate_and_clean(self.data['Bundles'][i]['Name']) + " UNKNOWN ERROR, NO ID\n")
            else:
                for o in range(0, len(self.data['Bundles'][i][ProductTag])):
                    isAvailable = False
                    if 'IsAvailable' in self.data['Bundles'][i][ProductTag][o]: # If article is available then print the right message with right values
                        self.write_in_csv("You can buy " + self.truncate_and_clean(self.data['Bundles'][i][ProductTag][o]['Name']) +
                        " at our store at ")
                        if 'Price' in self.data['Bundles'][i][ProductTag][o] and self.data['Bundles'][i][ProductTag][o]['Price'] is not None: # Case if Price is null, we go for WasPrice value instead
                            self.write_in_csv(str(round(self.data['Bundles'][i][ProductTag][o]['Price'], 1)))
                        elif 'WasPrice' in self.data['Bundles'][i][ProductTag][o]:
                            self.write_in_csv(str(round(self.data['Bundles'][i][ProductTag][o]['WasPrice'], 1)))
                        self.write_in_csv('\n')
                        amount += 1
                        isAvailable = True
                # If there is an article but it is not available we show it's id and name
                if (isAvailable is not True):
                        self.write_in_csv(str(self.data['Bundles'][i][ProductTag][o]['Stockcode']) + ' ' + str(self.truncate_and_clean(self.data['Bundles'][i][ProductTag][o]['Name']) + '\n'))
        print(amount, "articles were on available.")

    def __del__(self):
        print('Closing the Store, have a nice day :)')

f = Store()
f.print_products()