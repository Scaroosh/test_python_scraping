import random

def random_gen():
    while (True): # It's a generator so it runs until we hit 15
        sum = random.randint(10, 20)
        yield (sum)
        if (sum == 15):
            break; # Stop

def decorator_to_str(func):
    def wrapper(*args, **kwargs): # *args, **kwargs make it accepts any amount of arguments
        return (str(func(*args, **kwargs))) # Returns the function's return but casted in string
    return wrapper


@decorator_to_str
def add(a, b):
    return a + b


@decorator_to_str
def get_info(d):
    return d['info']


def ignore_exception(exception):
    def decorator(function): # Get Function
        def wrapper(*args, **kwargs): # Get Arguments of the function
                try: # Try to do the function, if it works then just returns it
                    function(*args)
                    return (function(*args))
                except exception: # Else just returns None
                    return None
        return wrapper
    return decorator


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap



class MetaInherList(type):
    # todo exercise 5
    pass


class Ex:
    x = 4


class ForceToList(Ex, metaclass=MetaInherList):
    pass

