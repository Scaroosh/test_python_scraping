##### EXERCICE 1 #####

def get_case(i): # case matching if number is modulo 3, 5 or 3 and 5
    case = 0
    if i % 3 == 0:
        case = 1
    if i % 5 == 0:
        case = 2
    if i % 5 == 0 and i % 3 == 0:
        case = 3
    return (case)

def exercise_one():
    for i in range(1, 101): # Loop from 1 to 101
        match get_case(i): # Matching return from get_case to a value
            case 0:
                print(i)
            case 1:
                print("Three")
            case 2:
                print("Five")
            case 3:
                print("ThreeFive")

##### EXERCICE 2 #####

def exercise_two(number): # Only working for positive numbers
    products = []
    results = []
    converted_num = str(number) # Converting int to string for easier manipulation
    # Creating all the possible combinations of products from the number
    for start in range(len(converted_num)):
        for end in range(start, len(converted_num)):
            products.append(converted_num[start:end + 1])
    # Calculating every products into results
    for number in products:
        result = 1
        # Multiplying digits from a product
        for digit in number:
            result *= int(digit)
        results.append(result)
    # Checking if the length of a set of unique results is equal to the length of our results
    if(len(set(results)) == len(results)):
        return (True)
    return (False)

##### EXERCICE 3 #####

def checkInt(str):
    if str[0] in '-':
        return str[1:].isdigit()
    return str.isdigit()

def calculate(items):
    sum = 0
    if type(items) != list: # Checking type of argument, useless to keep going if it isn't a list
        return (False)
    # For every item in items, check it's type, we only add string, and only if the string can be an int
    for item in items:
        if (type(item) == str):
            if checkInt(item):
                sum += int(item)
    return sum

##### EXERCICE 4 #####

def anagrams(str, items): # str is a string, items a list
    ana_list = []
    for item in items:
        if sorted(str) == sorted(item): # Checking if sorted str is equal to sorted item in items (For example abba => aabb)
            ana_list.append(item)
    return (ana_list)